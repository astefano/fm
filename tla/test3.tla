------------------------------ MODULE test3 ------------------------------
EXTENDS Integers, FiniteSets, TLC
CONSTANT Proposal, N, F 
Node == 0..N-1
Correct == F..N-1
Byz == 0..F-1

ASSUME N \in Nat /\ F \in Nat (*/\ (N > 2*F +1)*) 

Message ==      [type : {"prepropose"}, sender : Node, receiver : Node, p : Proposal, ts : Nat]
(*           \cup [type : {"byzprepropose"}, sender : Node, receiver : Node, p : Proposal] *)
           \cup [type : {"recv_prepropose"}, receiver : Node, p : Proposal, ts : Nat]
           \cup [type : {"propose"}, sender : Node, receiver : Node, p : Proposal, ts : Nat]
           \cup [type : {"recv_propose"}, receiver : Node, ps : SUBSET Proposal, ts : Nat]
           \cup [type : {"finished"}]
                       
VARIABLE p, dec, msgs, ts

vars == <<p, dec, msgs, ts>>

Max(s) == CHOOSE x \in s : \A y \in s : x >= y

Send(m) == msgs' = msgs \cup {m}

TypeOK == msgs \subseteq Message

Init == /\ p = [n \in Node |-> 0]
        /\ dec = [n \in Node |-> 0]
        /\ msgs = {}
        /\ ts = 0
        
ByzSendPropose(i, j, pv) == Send([type |-> "propose", sender |-> i, receiver |-> j, p |-> pv, ts |-> ts])
                           /\ p' = [ p EXCEPT ![i] = pv ]
                           /\ ts' = ts + 1
                           /\ UNCHANGED <<dec>>        
 
SendPrePropose(i, pv) ==  LET preproposes_msgs == {m \in msgs : m.type = "prepropose"} IN
                          /\ Cardinality(preproposes_msgs) = 0    
                          /\ Send([type |-> "prepropose", sender |-> i, p |-> pv, ts |-> ts])
                          /\ p' = [ p EXCEPT ![i] = pv ]
                          /\ ts' = ts + 1
                          /\ UNCHANGED <<dec>>
                          
RecvPrePropose(i) == \E m \in msgs :
                     /\ m.type = "prepropose"  
                     /\ p' = [ p EXCEPT ![i] = m.p ]
                     /\ Send([type |-> "recv_prepropose", receiver |-> i, p |-> m.p, ts |-> ts])
                     /\ ts' = ts + 1
                     /\ UNCHANGED <<dec>>
                     
SendPropose(i, j) == \E m \in msgs : 
                  /\ m.type = "recv_prepropose" /\ m.receiver = i /\ m.p = p[i]
                  /\ Send([type |-> "propose", sender |-> i, receiver |-> j, p |-> p[i], ts |-> ts])
                  /\ ts' = ts + 1
                  /\ UNCHANGED <<p, dec>>                   

RecvPropose(i) == LET proposes_msgs == {m \in msgs : m.type = "propose" /\ m.receiver = i} IN
                  LET propose_vals == {m.p : m \in proposes_msgs} IN  
                  /\ Cardinality(proposes_msgs) = N-F 
                  /\ dec' = [dec EXCEPT ![i] = Max(propose_vals)]
                  /\ ts' = ts + 1 
                  /\ Send([type |-> "recv_propose", receiver |-> i, ps |-> propose_vals, ts |-> ts])
                  /\ UNCHANGED <<p>>
            
Next == \E pv \in Proposal : 
        \/ \E i \in Correct : \A j \in Correct :  \/ SendPrePropose(i, pv) 
                               \/ RecvPrePropose(i)
                               \/ SendPropose(i, j) 
                               \/ RecvPropose(i)
       \/ \E i \in Byz : \E j \in Correct : ByzSendPropose(i, j, pv)                               

Spec == Init /\ [][Next]_vars

LiveSpec == Spec /\ WF_vars(Next)

(*
WFRecvPrepropose == \A i \in Node : WF_vars(RecvPrePropose(i))
WFSendPrepropose == \A pv \in Proposal : \A i \in Node : WF_vars(SendPrePropose(i, pv))
WFSendPropose == \A i \in Node : WF_vars(SendPropose(i))
WFRecvPropose == \A i \in Node : WF_vars(RecvPropose(i))
WFPropose == \A i \in Node : WF_<<>>(SendPropose(i))

Spec == Init /\ [][Next]_vars /\ WFRecvPropose /\ WFSendPropose /\ WFSendPrepropose /\ WFRecvPrepropose*)

Agreed == \A i \in Correct : \A j \in Correct : dec[i] > 0 /\ dec[j] > 0 => dec[i] = dec[j]

P0 == \A i \in Correct : <> (dec[i] > 0)
(* P0 == \A i \in Node : [] (<> <<RecvVote(i)>>_vars => <> (dec[i] > 0)) *)

(* P1 is false: there is a state where all dec[i] > 0 *)
P1 == \neg (\A i \in Correct : \A j \in Correct : dec[i] = dec[j] /\ dec[i] > 0)

P2 == \neg <> (dec[1] = 2 /\ dec[2] = 2)

CEX == {[type |-> "prepropose", sender |-> 1, p |-> 1, ts |-> 0],
        [type |-> "recv_prepropose", receiver |-> 1, p |-> 1, ts |-> 1],   
        [type |-> "propose", sender |-> 1, receiver |-> 2, p |-> 1, ts |-> 2],
        [type |-> "propose", sender |-> 0, receiver |-> 2, p |-> 2, ts |-> 3],
        [type |-> "recv_propose", sender |-> 1, ps |-> {1, 2}, ts |-> 4]}
        
P3 == \neg <> (CEX \subseteq msgs)
=============================================================================

