------------------------------ MODULE test0 ------------------------------
EXTENDS Integers, FiniteSets, TLC
CONSTANT Proposal, N 
ASSUME N \in Nat

Node == 0..N-1
(* Epoch ==  0..1 *)
(* Height == 0..0 *)

Message ==      [type : {"prepropose"}, sender : Node, p : Proposal]
           \cup [type : {"recv_prepropose"}, receiver : Node, p : Proposal]
           \cup [type : {"propose"}, sender : Node, p : Proposal]
           \cup [type : {"recv_propose"}, receiver : Node, p : Proposal]
           \cup [type : {"vote"}, sender : Node, v : Proposal]
           \cup [type : {"recv_vote"}, receiver : Node, p : Proposal]

VARIABLE p, v, dec, msgs

vars == <<p, v, dec, msgs>>

Max(s) == CHOOSE x \in s : \A y \in s : x >= y

Send(m) == msgs' = msgs \cup {m}

TypeOK == msgs \subseteq Message

Init == (* /\ sender = [n \in Node |-> n] *)
        (* /\ e = [n \in Node |-> 0]
        /\ h = [n \in Node |-> 0] *)
        /\ p = [n \in Node |-> 0]
        /\ v = [n \in Node |-> 0]
        /\ dec = [n \in Node |-> 0]
        /\ msgs = {}

(* isProposer(i) == h[i] % N = i *) 
 
SendPrePropose(i, pv) ==  (* /\ isProposer(i) *)
                          LET preproposes_msgs == {m \in msgs : m.type = "prepropose"} IN
                          /\ Cardinality(preproposes_msgs) = 0    
                          /\ Send([type |-> "prepropose", sender |-> i, p |-> pv])
                          /\ p' = [ p EXCEPT ![i] = pv ]
                          (* /\ Print (msgs, TRUE) *)
                          /\ UNCHANGED <<v, dec>>
                   
RecvPrePropose(i) == \E m \in msgs :
                     /\ m.type = "prepropose"
                     /\ p' = [ p EXCEPT ![i] = m.p ]
                     /\ Send([type |-> "recv_prepropose", receiver |-> i, p |-> m.p])
                     (* /\ Print (msgs, TRUE) *)
                     /\ UNCHANGED <<v, dec>>  
                 
SendPropose(i) == \E m \in msgs : 
                  /\ m.type = "recv_prepropose" /\ m.receiver = i /\ m.p = p[i]
                  /\ Send([type |-> "propose", sender |-> i, p |-> p[i]])
                  (* /\ Print (msgs, TRUE) *)
                  /\ UNCHANGED <<p, v, dec>>                   

RecvPropose(i) == LET proposes_msgs == {m \in msgs : m.type = "propose"} IN
                  LET propose_vals == {m.p : m \in proposes_msgs} IN  
                  /\ Cardinality(proposes_msgs) = N 
                  /\ v' = [v EXCEPT ![i] = Max(propose_vals)] 
                  /\ Send([type |-> "recv_propose", receiver |-> i, p |-> propose_vals])
                  (* /\ Print (msgs, TRUE) *)
                  /\ UNCHANGED <<p, dec>>
                
SendVote(i) == \E m \in msgs : 
                /\ m.type = "recv_propose" /\ m.receiver = i 
                /\ Send([type |-> "vote", sender |-> i, v |-> v[i]])
                (* /\ Print (msgs, TRUE) *)
                /\ UNCHANGED <<p, v, dec>>                   

RecvVote(i) == LET vote_msgs == {m \in msgs : m.type = "vote"} IN
               LET vote_vals == {m.v : m \in vote_msgs} IN  
               /\ Cardinality(vote_msgs) = N 
               /\ dec' = [dec EXCEPT ![i] = Max(vote_vals)]
               /\ Send([type |-> "recv_vote", receiver |-> i, p |-> vote_vals])
               (* /\ h' = [h EXCEPT ![i] = h[i]+1] *)
               (* /\ Print (msgs, TRUE) *) 
               /\ UNCHANGED <<p, v>> 

Next == \E pv \in Proposal : 
        \/ \E i \in Node :  \/ SendPrePropose(i,pv) 
                            \/ RecvPrePropose(i)
                            \/ SendPropose(i) 
                            \/ RecvPropose(i)
                            \/ SendVote(i) 
                            \/ RecvVote(i) 

WFRecvVote == \A i \in Node : WF_<<>>(RecvVote(i))

WFRecvVote0 == WF_<<>>(RecvVote(0)) /\ WF_<<>>(RecvVote(1))

WFRecvVote1 == WF_<<msgs,dec>>(\A i \in Node : RecvVote(i))
 
Spec == Init /\ [][Next]_vars /\ WFRecvVote

Agreed == \A i \in Node : \A j \in Node : dec[i] > 0 /\ dec[j] > 0 => dec[i] = dec[j]

P0 == \A i \in Node : <> (dec[i] > 0)
(* P0 == \A i \in Node : [] (<> <<RecvVote(i)>>_vars => <> (dec[i] > 0)) *)

(* P1 is false: there is a state where all dec[i] > 0 *)
P1 == \neg (\A i \in Node : \A j \in Node : dec[i] = dec[j] /\ dec[i] > 0) 
=============================================================================

