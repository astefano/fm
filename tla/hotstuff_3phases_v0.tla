------------------------------ MODULE hotstuff_3phases_v0 ------------------------------
EXTENDS Integers, FiniteSets, TLC, Sequences
(* CONSTANT Proposal, Epoch, N, F *) 
(* ASSUME N \in Nat /\ F \in Nat *)

Views == 0..4
Proposals == {1, 2, 3, 4}
F == 1
T == 1
N == 4

Nodes == 1..N
Byz == 1..F
Correct == F+1..N

Threshold == N - T
AtLeastT2(S) == Cardinality(S) >= Threshold 

(* APALACHE-BEGIN annotations *)
a <: b == a

(* msg type for prepared/(pre)committed *)
MT == [type |-> STRING, sender |-> Int, v |-> Int, p |-> Int]
(* certificate type *)
QT == {MT}
(* msg type for newleader *)
MT_leader == [type |-> STRING, sender |-> Int, curr_view |-> Int, prepared_view |-> Int, prepared_val |-> Int, cert |-> QT]
(* msg type for proposal *)
MT_propose == [type |-> STRING, sender |-> Int, v |-> Int, p |-> Int, cert |-> QT]
PT1 == [e |-> Int, p |-> Int]
PT2 == <<Int, Int>>

(* APALACHE-END *)

(*
Messages ==     ([type : {"newleader"}, sender : Nodes, curr_view : Views, prepared_view : Views, prepared_val : Proposals, cert : QT] <: {MT_leader})
           \cup ([type : {"propose"}, sender : Nodes, v : Views, p : Proposals, cert : QT] <: {MT_propose})
           \cup ([type : {"prepared"}, sender : Nodes, v : Views, p : Proposals] <: {MT})
           \cup ([type : {"precommitted"}, sender : Nodes, v : Views, p : Proposals] <: {MT})
           \cup ([type : {"committed"}, sender : Nodes, v : Views, p : Proposals] <: {MT})
*)

(* Byz propose and vote any value *)
(* ByzProposals == [type : {"propose"}, sender: Byz, v: Viewa, p: Proposals, cert: ???]
ByzVotes == [type : {"vote"}, sender: Byz, p: Proposals, e : Epochs, h : Heights2] 
ByzMsg == ByzProposals \cup ByzVotes *)

ByzPrepared == [type : {"prepared"}, sender : Byz, v : Views, p : Proposals] <: {MT}
ByzPrecomm == [type : {"precommitted"}, sender : Byz, v : Views, p : Proposals] <: {MT}
ByzComm == [type : {"committed"}, sender : Byz, v : Views, p : Proposals] <: {MT}
            
(* QCs are sets of prepared_msgs *)
QCs == {MT} <: {QT}
           
VARIABLE p, 
         dec, 
         curr_view, 
         curr_val,
         prepared_view, 
         prepared_val, 
         locked_view, 
         locked_val, 
         cert,
         msgsNewleader,
         msgsPropose,
         msgsPrepared, 
         msgsPrecommitted,
         msgsCommitted,         
         qcs,
         voted

vars == <<p, dec, curr_view, curr_val, prepared_view, prepared_val, locked_view, locked_val, qcs, voted, cert, msgsNewleader, msgsPropose, msgsPrepared, msgsPrecommitted, msgsCommitted>>

Max(s) == CHOOSE x \in s : \A y \in s : x >= y

Min(s) == CHOOSE x \in s : \A y \in s : y >= x

Add(qc) == qcs' = qcs \cup {qc}

(* update this for msgs_X *)
(* TypeOK == msgs \subseteq Messages
          /\ qcs \subseteq QCs *)

(* Send(m) == msgs' = msgs \cup m *)
         
          
Init == /\ p = [n \in Nodes |-> n]
        /\ curr_view = [n \in Nodes |-> 0]
        /\ curr_val = [n \in Nodes |-> 0]
        /\ prepared_view = [n \in Nodes |-> 0]
        /\ locked_view = [n \in Nodes |-> 0]
        /\ prepared_val = [n \in Nodes |-> 0]
        /\ locked_val = [n \in Nodes |-> 0]
        /\ cert = [n \in Nodes |-> {} <: QT]   
        /\ voted = [n \in Nodes |-> FALSE]    
        (* -1 denotes undefined *)
        /\ dec = [n \in Nodes |-> -1]
        (* todo: need to add ByzMsg *)
        (* q: in tendermint, msgPrecommit for instance is 
        the set of precommits broadcast in the system as a
        function of height & round. similarly, in our case, a fct on views *) 
        /\ msgsNewleader = [v1 \in Views |-> {} <: {MT_leader}] 
        /\ msgsPropose = [v1 \in Views |-> {} <: {MT_propose}] 
        /\ msgsPrepared = ([v1 \in Views |-> ByzPrepared <: {MT}])  
        /\ msgsPrecommitted = ([v1 \in Views |-> ByzPrecomm <: {MT}]) 
        /\ msgsCommitted = ([v1 \in Views |-> ByzComm <: {MT}]) 
        /\ qcs = {} <: {QT} 
         
Leader(k) == (k % N)    

(* Preds from Fig. 5 *)
(* nx stands for new x *)     
Prepared(ncert, nv, np) ==    AtLeastT2(ncert) 
                          (* "each msg in cert is prepared(_, nv, np)" *)   
                          /\ \A c \in ncert : c.type = "prepared" /\ c.v = nv /\ c.p = np 

ValidNewLeader(m) ==     m.type = "newleader" 
                     /\  m.curr_view > m.prepared_view 
                     /\ (m.prepared_view = 0 \/ Prepared(m.cert, m.prepared_view, m.prepared_val)) 

SafeProposal(m, i) ==    m.type = "propose" 
                      /\ m.sender = Leader(m.v) 
                      /\ (    locked_view[i] = 0 
                          \/ (    prepared_val[i] = m.p 
                              \/ (\E v1 \in Views : 
                                      locked_view[i] < v1 
                                      /\ v1 < m.v /\ Prepared(m.cert, v1, m.p)))) 
 
OnView(nv, i) ==    curr_view' = [curr_view EXCEPT ![i] = nv]
                 /\ voted' = [voted EXCEPT ![i] = FALSE]
                 /\ LET newLeaderMsg == [type |-> "newleader", 
                                         sender |-> i, 
                                         curr_view |-> nv, 
                                         prepared_view |-> prepared_view[i], 
                                         prepared_val |-> prepared_val[i], 
                                         cert |-> cert[i] <: QT] <: MT_leader
                    IN msgsNewleader' = [msgsNewleader EXCEPT ![nv] = msgsNewleader[nv] \cup {newLeaderMsg}]
                 /\ UNCHANGED <<p, dec, curr_val, prepared_view, prepared_val, locked_view, locked_val, qcs, cert, msgsPropose, msgsPrepared, msgsPrecommitted, msgsCommitted>>                     

(*
OnNewleader(i) == 
     i = Leader(curr_view[i])
  /\ LET msgsNewleaderForCurrView == {m \in msgsNewleader[curr_view[i]] : 
                                         m.type = "newleader" 
                                      /\ m.curr_view = curr_view[i] 
                                      } IN                  
     AtLeastT2(msgsNewleaderForCurrView)  
  /\ \A m \in msgsNewleaderForCurrView : ValidNewLeader(m)  
  /\ LET newProposeMsg == [type |-> "propose", 
                           sender |-> i, 
                           v |-> curr_view[i], 
                           p |-> p[i], 
                           cert |-> {} <: QT] <: MT_propose
     IN msgsPropose' = [msgsPropose EXCEPT ![curr_view[i]] = msgsPropose[curr_view[i]] \cup {newProposeMsg}]   
  /\ UNCHANGED  <<p, dec, curr_view, curr_val, prepared_view, prepared_val, locked_view, locked_val, qcs, voted, cert, msgsNewleader, msgsPrepared, msgsPrecommitted, msgsCommitted>>
*)

OnNewleader(i) == 
     i = Leader(curr_view[i])
  /\ LET msgsNewleaderForCurrView == {m \in msgsNewleader[curr_view[i]] : 
                                         m.type = "newleader" 
                                      /\ m.curr_view = curr_view[i] 
                                      } IN                  
     AtLeastT2(msgsNewleaderForCurrView)  
  /\ \A m \in msgsNewleaderForCurrView : ValidNewLeader(m)   
  /\ \E m1 \in msgsNewleaderForCurrView :
        m1.prepared_view = Max({vm.prepared_view : vm \in msgsNewleaderForCurrView})  
  /\ LET newProposeMsg == [type |-> "propose", 
                           sender |-> i, 
                           v |-> curr_view[i], 
                           p |-> IF m1.prepared_view > 0 THEN m1.prepared_val ELSE p[i], 
                           cert |-> IF m1.prepared_view > 0 THEN m1.cert <: QT ELSE {} <: QT] <: MT_propose
     IN msgsPropose' = [msgsPropose EXCEPT ![curr_view[i]] = msgsPropose[curr_view[i]] \cup {newProposeMsg}]   
  /\ UNCHANGED  <<p, dec, curr_view, curr_val, prepared_view, prepared_val, locked_view, locked_val, qcs, voted, cert, msgsNewleader, msgsPrepared, msgsPrecommitted, msgsCommitted>>

OnPropose(i) ==
     \neg voted[i] 
  /\ LET msgsProposeForCurrView == {m \in msgsPropose[curr_view[i]] : 
                                         m.type = "propose" 
                                      /\ m.v = curr_view[i] 
                                      } IN 
  (* /\ LET good_ms = { m : m \in msgsProposeForCurrView /\ SafeProposal(m, i)} 
     /\ Cardinality(good_ms) < 2
  *)
  \E m \in msgsProposeForCurrView : SafeProposal(m, i)
    /\ curr_val' = [curr_val EXCEPT ![i] = m.p]
    /\ voted' = [voted EXCEPT ![i] = TRUE]
    /\ LET newPreparedMsg == 
                          [type |-> "prepared", 
                           sender |-> i, 
                           v |-> curr_view[i], 
                           p |-> curr_val[i]] <: MT
       IN msgsPrepared' = [msgsPrepared EXCEPT ![curr_view[i]] = msgsPrepared[curr_view[i]] \cup {newPreparedMsg}]   
    /\ UNCHANGED  <<p, dec, curr_view, locked_view, locked_val, prepared_view, prepared_val, qcs, cert, msgsNewleader, msgsPropose, msgsPrecommitted, msgsCommitted>>
  
OnPrepared(i) ==
     voted[i] 
  /\ LET msgsPreparedForCurrView == {m \in msgsPrepared[curr_view[i]] : 
                                         m.type = "prepared" 
                                      /\ m.v = curr_view[i] 
                                      } IN                  
     AtLeastT2(msgsPreparedForCurrView)
  /\ prepared_val' = [prepared_val EXCEPT ![i] = curr_val[i]]
  /\ prepared_view' = [prepared_view EXCEPT ![i] = curr_view[i]]
  /\ cert' = [cert EXCEPT ![curr_view[i]] = msgsPreparedForCurrView]   
  /\ LET newPrecommittedMsg == 
                          [type |-> "precommitted", 
                           sender |-> i, 
                           v |-> curr_view[i], 
                           p |-> curr_val[i]] <: MT
     IN msgsPrecommitted' = [msgsPrecommitted EXCEPT ![curr_view[i]] = msgsPrecommitted[curr_view[i]] \cup {newPrecommittedMsg}]   
  /\ UNCHANGED  <<p, dec, curr_view, curr_val, locked_view, locked_val, qcs, voted, msgsNewleader, msgsPrepared, msgsPropose, msgsCommitted>>

OnPrecommitted(i) ==
  LET msgsPrecommittedForCurrView == {m \in msgsPrecommitted[curr_view[i]] : 
                                         m.type = "precommitted" 
                                      /\ m.v = curr_view[i] 
                                      /\ m.v = prepared_view[i]  
                                      /\ m.p = curr_val[i] 
                                      } IN                  
      AtLeastT2(msgsPrecommittedForCurrView) 
  /\ locked_view' = [locked_view EXCEPT ![i] = prepared_view[i]]
  /\ LET newCommittedMsg == 
                          [type |-> "committed", 
                           sender |-> i, 
                           v |-> curr_view[i], 
                           p |-> prepared_val[i]] <: MT
     IN msgsCommitted' = [msgsCommitted EXCEPT ![curr_view[i]] = msgsCommitted[curr_view[i]] \cup {newCommittedMsg}]   
  /\ UNCHANGED  <<p, dec, curr_view, curr_val, prepared_view, prepared_val,              locked_val, qcs, voted, cert, msgsNewleader, msgsPrepared, msgsPropose, msgsPrecommitted>>

OnCommitted(i) ==
  LET msgsCommittedForCurrView == {m \in msgsCommitted[curr_view[i]] : 
                                         m.type = "committed" 
                                      /\ m.v = curr_view[i] 
                                      /\ m.v = locked_view[i]  
                                      /\ m.p = curr_val[i] 
                                      } IN                  
      AtLeastT2(msgsCommittedForCurrView) 
  /\ dec' = [dec EXCEPT ![i] = curr_val[i]]
  /\ UNCHANGED  <<p, curr_view, curr_val, prepared_view, prepared_val, locked_view, locked_val, qcs, voted, cert, msgsNewleader, msgsPrepared, msgsPropose, msgsPrecommitted, msgsCommitted>>
                                                               
Next == \E nv \in Views, i \in Correct : OnView(nv, i) \/ OnNewleader(i) \/ OnPropose(i) \/ OnPrepared(i) \/ OnPrecommitted(i) \/ OnCommitted(i)  
 
(* Next == \E i \in Correct : OnView(1, i) \/ OnPropose(i) *) 
 
Spec == Init /\ [][Next]_vars 

LiveSpec == Spec /\ WF_vars(Next)

Agreed == \A i \in Correct : \A j \in Correct : dec[i] > -1 /\ dec[j] > -1 => dec[i] = dec[j]

CEX == {[type |-> "newleader", sender |-> 1, curr_view |-> 1, prepared_view |-> -1, prepared_val |-> -1, cert |-> {}], 
        [type |-> "newleader", sender |-> 2, curr_view |-> 1, prepared_view |-> -1, prepared_val |-> -1, cert |-> {}],
        [type |-> "newleader", sender |-> 3, curr_view |-> 1, prepared_view |-> -1, prepared_val |-> -1, cert |-> {}]}
        
P == \neg (CEX \subseteq msgsNewleader[1])

CEX1 == {[type |-> "propose", sender |-> 2, v |-> 2, p |-> 2, cert |-> {}]}
        
P1 == \neg (CEX \subseteq msgsPropose[1])

P2 == Cardinality(msgsPropose[2]) < 1

P3 == Cardinality(msgsPrepared[1]) < 1

P4 == Cardinality(msgsPrecommitted[1]) < 3

P5 == Cardinality(msgsCommitted[1]) < 1

P6 == Cardinality(msgsCommitted[1]) < 3

P7 == \A i \in Correct : dec[i] < 0
=============================================================================