------------------------------ MODULE test2 ------------------------------
(* Simple consensus; symmetric Byz
   - the behaviour of the symByz is implicit : the WF is only for the correct nodes 
 *)

EXTENDS Integers, FiniteSets, TLC
CONSTANT Proposal, N, F 
ASSUME N \in Nat /\ F \in Nat

Node == 0..N-1
Correct == F..N-1

Message ==      [type : {"prepropose"}, sender : Node, p : Proposal]
           \cup [type : {"recv_prepropose"}, receiver : Node, p : Proposal]
           \cup [type : {"propose"}, sender : Node, p : Proposal]
           \cup [type : {"recv_propose"}, receiver : Node, ps : SUBSET Proposal]
           
VARIABLE p, dec, msgs

vars == <<p, dec, msgs>>

Max(s) == CHOOSE x \in s : \A y \in s : x >= y

Send(m) == msgs' = msgs \cup {m}

TypeOK == msgs \subseteq Message

Init == /\ p = [n \in Node |-> 0]
        /\ dec = [n \in Node |-> 0]
        /\ msgs = {}
 
SendPrePropose(i, pv) ==  LET preproposes_msgs == {m \in msgs : m.type = "prepropose"} IN
                          (* only one sends prepropose *)  
                          /\ Cardinality(preproposes_msgs) = 0     
                          /\ Send([type |-> "prepropose", sender |-> i, p |-> pv])
                          /\ p' = [ p EXCEPT ![i] = pv ]
                          /\ UNCHANGED <<dec>>
                          
RecvPrePropose(i) == \E m \in msgs :
                     /\ m.type = "prepropose"
                     /\ p' = [ p EXCEPT ![i] = m.p ]
                     /\ Send([type |-> "recv_prepropose", receiver |-> i, p |-> m.p])
                     /\ UNCHANGED <<dec>>
                     
SendPropose(i) == \E m \in msgs : 
                  /\ m.type = "recv_prepropose" /\ m.receiver = i /\ m.p = p[i]
                  /\ Send([type |-> "propose", sender |-> i, p |-> p[i]])
                  /\ UNCHANGED <<p, dec>>                   

RecvPropose(i) == LET proposes_msgs == {m \in msgs : m.type = "propose"} IN
                  LET propose_vals == {m.p : m \in proposes_msgs} IN  
                  /\ Cardinality(proposes_msgs) = N - F 
                  /\ dec' = [dec EXCEPT ![i] = Max(propose_vals)] 
                  /\ Send([type |-> "recv_propose", receiver |-> i, ps |-> propose_vals])
                  /\ UNCHANGED <<p>>

Next == \E pv \in Proposal : 
        \/ \E i \in Correct :  \/ SendPrePropose(i,pv) 
                               \/ RecvPrePropose(i)
                               \/ SendPropose(i) 
                               \/ RecvPropose(i)
 
Spec == Init /\ [][Next]_vars 

LiveSpec == Spec /\ WF_vars(Next)

Agreed == \A i \in Correct : \A j \in Node : dec[i] > 0 /\ dec[j] > 0 => dec[i] = dec[j]

P0 == \A i \in Correct : <> (dec[i] > 0)
(* P0 == \A i \in Node : [] (<> <<RecvVote(i)>>_vars => <> (dec[i] > 0)) *)

(* P1 is false: there is a state where all dec[i] > 0 *)
P1 == \neg (\A i \in Correct : dec[i] > 0)

CEX == {[type |-> "recv_propose", receiver |-> 2, ps |-> {1, 2}]}
        
P2 == \neg (CEX \subseteq msgs) 
=============================================================================

