------------------------------ MODULE streamlet_v0 ------------------------------
EXTENDS Integers, FiniteSets, TLC, Sequences
(* CONSTANT Proposal, Epoch, N, F *) 
(* ASSUME N \in Nat /\ F \in Nat *)

(** 1st version of streamlet **)
(** check if finalising in 2 blks instead of 3 breaks safety **)

(* phase 0 is propose, phase 1 is vote *)
Phases == 0..1
Epochs == 0..2
Heights == 0..2
Heights1 == 0..1
Heights2 == 1..2
Proposals == {1, 2, 3, 4}
F == 2
T == 1
N == 4

Nodes == 1..N
Correct == 1..N-F
Byz == N-F+1..N

Threshold == N - T
AtLeastT2(S) == Cardinality(S) >= Threshold

(* APALACHE-BEGIN annotations *)
a <: b == a

MT == [type |-> STRING, sender |-> Int, p |-> Int, e |-> Int, h |-> Int]

(* APALACHE-END *)

Messages ==      ([type : {"propose"}, sender : Nodes, p : Proposals, e : Epochs, h : Heights] <: {MT})
           \cup ([type : {"vote"}, sender : Nodes, p : Proposals, e : Epochs, h : Heights] <: {MT})

(* Byz propose and vote any value *)
ByzProposals == [type : {"propose"}, sender: Byz, p: Proposals, e : Epochs, h : Heights2]
ByzVotes == [type : {"vote"}, sender: Byz, p: Proposals, e : Epochs, h : Heights2] 
ByzMsg == ByzProposals \cup ByzVotes

QCs == {[signers: SUBSET Nodes, pv : Proposals, e : Epochs, h : Heights]}
           
(* qcs is the set of quorum certificates *)
(* chain[i][h] is a set of notarized blocks at height h for process i; a block is a pair (e, v) e is the epoch, v is the value *)           
VARIABLE p, phase, e, h, dec, chain, msgs, qcs

vars == <<p, phase, e, h, dec, chain, msgs, qcs>>

Max(s) == CHOOSE x \in s : \A y \in s : x >= y

Min(s) == CHOOSE x \in s : \A y \in s : y >= x

(* the maximal height in chain[i] where there are notarized blocks *)
MaxH(i) == CHOOSE x \in Heights1 : Cardinality(chain[i][x]) > 0 /\ chain[i][x+1] = {} 

Send(m) == msgs' = msgs \cup m

Add(qc) == qcs' = qcs \cup {qc}

TypeOK == msgs \subseteq Messages
          /\ qcs \subseteq QCs
          
Init == /\ p = [n \in Nodes |-> n]
        /\ e = [n \in Nodes |-> 0]
        /\ h = [n \in Nodes |-> 0]
        /\ phase = [n \in Nodes |-> 1]
        (* -1 denotes undefined *)
        /\ dec = [n \in Nodes |-> [ht \in Heights |-> -1]]
        /\ chain = [n \in Nodes |-> [ht \in Heights |-> {}]]
        /\ msgs = ByzMsg
        /\ qcs = {}
         
Proposer(k) == (1 + (k % N))    
         
OnNotarise(i) == /\ h[i] + 1 \in Heights
                 /\ \E pv \in Proposals, ev \in Epochs, hv \in Heights :                     
                   LET votes == {m \in msgs : m.type = "vote" /\ m.e = ev /\ m.p = pv /\ m.h = hv} IN
                   /\ LET qc == 
                        IF (AtLeastT2(votes)) THEN {[signers |-> {v.sender : v \in votes}, pv |-> pv, e |-> ev, h |-> hv]}
                        ELSE {}
                      IN
                      (* FIXME: for the moment, qcs may contain both A, B where A \subset B *)  
                      qcs' = qcs \cup qc                   
                   /\ h' = IF (AtLeastT2(votes)) THEN [h EXCEPT ![i] = hv] ELSE h
                   /\ LET notarizedBlks ==  
                        IF (AtLeastT2(votes)) THEN chain[i][hv] \cup {<<ev,pv>>} ELSE chain[i][hv]
                      IN
                      chain' = [chain EXCEPT ![i][hv] = notarizedBlks]
                   (* /\ Print("qcs " \cup qcs, TRUE) *)
                   /\ UNCHANGED <<p, msgs, dec, e, phase>> 
         
OnPropose(i) ==  (* /\ OnNotarise(i) *)
                 /\ h[i] + 1 \in Heights
                 /\ phase' = [phase EXCEPT ![i] = 1]                
                 /\ LET maxh == IF chain[i][1] = {} THEN 1 ELSE MaxH(i) + 1
                   IN                
                   /\ h' = IF i = Proposer(e[i]) THEN [h EXCEPT ![i] = maxh] ELSE h 
                   /\   LET proposeM ==
                         IF i = Proposer(e[i])
                            THEN {[type |-> "propose", sender |-> i, p |-> p[i], e |-> e[i], h |-> maxh] <: MT}
                            ELSE {} <: {MT}
                     IN
                     (* should check if already sent for e, h? *) 
                     msgs' = msgs \cup proposeM
                 (* /\ LET proposed_heights == 
                        {m.h : m \in {m \in msgs : m.type = "propose"}} 
                    IN                    
                    h' = IF Cardinality(proposed_heights) > 0 THEN [h EXCEPT ![i] = Max(proposed_heights)] ELSE h *)
                 (* /\ Print("propose " \o ToString(i) \o " " \o ToString(h[i]), TRUE) *)            
                    /\ UNCHANGED <<p, e, dec, chain, qcs>>
    
OnVote(i) == (* /\ OnNotarise(i) *)
             /\ phase[i] = 1
             /\ \E pv \in Proposals, hv \in Heights:
               /\ ([type |-> "propose", sender |-> Proposer(e[i]), p |-> pv, e |-> e[i], h |-> hv]) \in msgs
               (* check if the block at hv-1 is genesis or notarized *)
               /\ (hv <= 1 \/ (hv > 1 /\ Cardinality(chain[i][hv-1]) > 0))
               (* check if no conflicting blk at hv! *)               
               /\ chain[i][hv] = {}               
               /\ Send({[type |-> "vote", sender |-> i, p |-> pv, e |-> e[i], h |-> hv]})
               /\ phase' = [phase EXCEPT ![i] = 0]
               /\ e' = [e EXCEPT ![i] = e[i]+1]                                                                     
               /\ UNCHANGED <<p, qcs, dec, chain, h>>                                        
                                               
OnFinalise(i) == (* /\ h[i] + 1 \in Heights *)
                 /\ \E ev \in Epochs, hv \in Heights1 : 
                   LET conseq_qcs == {<<qc1, qc2>> \in qcs \X qcs :  qc1.e = ev  /\ qc2.e = ev + 1 /\ qc1.h = hv /\ qc2.h = hv + 1} IN 
                   (* LET conseq_qcs == {<<qc1, qc2>> \in qcs \X qcs :  qc1.e = e[i]-1  /\ qc2.e = e[i]} IN *) 
                   LET pvs == { qc.pv : <<qc, qc1>> \in conseq_qcs } IN
                   (Cardinality(pvs) > 0)   
                   (* should be only 1 matching pair? *)
                   /\ dec' = [dec EXCEPT ![i][hv] = Min(pvs)] 
                   /\ h' = [h EXCEPT ![i] = hv + 1] 
                   /\ UNCHANGED <<p, phase, e, chain, msgs, qcs>>
                                               
Next == \E i \in Correct : (OnPropose(i) \/ OnVote(i) \/ OnNotarise(i) \/ OnFinalise(i))
 
Spec == Init /\ [][Next]_vars 

LiveSpec == Spec /\ WF_vars(Next)

Agreed == \A i \in Correct : \A j \in Correct : \A hv \in Heights1 : dec[i][hv] > 0 /\ dec[j][hv] > 0 => dec[i][hv] = dec[j][hv]

P0 == \A i \in Correct : <> (dec[i][h[i]] > 0)
(* P0 == \A i \in Node : [] (<> <<RecvVote(i)>>_vars => <> (dec[i] > 0)) *)

(* P1 is false: there is a state where all dec[i] > 0 *)
P1 == \neg (\A i \in Correct: \E hv \in Heights : dec[i][hv] > -1)

CEX1 == {[type |-> "propose", sender |-> 1, e |-> 0, p |-> 1, h |-> 1]}
CEX2 == {[type |-> "propose", sender |-> 2, e |-> 1, p |-> 2, h |-> 2]} 
CEX3 == {[type |-> "vote", sender |-> 2, e |-> 0, p |-> 1, h |-> 2], [type |-> "vote", sender |-> 1, e |-> 1, p |-> 2, h |-> 2]}
CEX4 == {[type |-> "vote", sender |-> 2, e |-> 1, p |-> 2, h |-> 2]} (* , [type |-> "vote", sender |-> 2, e |-> 1, p |-> 2], [type |-> "vote", sender |-> 3, e |-> 1, p |-> 2]}*)
        
P2 == \neg (CEX2 \subseteq msgs)

(* there is at least one notarization of (2, 2) *)
P4 == Cardinality({q \in qcs : q.h = 2 /\ q.pv = 2}) < 1

P3 == \neg (dec[1][1] > -1)

P5 == \neg (dec[1][1] > -1 /\ dec[2][1] > -1 /\ dec[3][1] > -1) 

P6 == \neg (dec[1][2] > -1)

CEX7 == {[signers |-> {1, 2, 3, 4}, e |-> 0, pv |-> 1, h |-> 1]}(*, 
       [signers |-> {1, 2, 3}, e |-> 1, pv |-> 2, h |-> 1]}*)

CEX8 == {[type |-> "propose", sender |-> 1, e |-> 0, p |-> 1, h |-> 1]}

P7 == \neg ((CEX7 \subseteq qcs))

(* both processes 1 and 2 decided 1 at h = 1 *)
P8 == \neg (dec[1][1] = 1 /\ dec[2][1] = 1)

(* it is not the case that process 1 decided 1 and process 2 decided 2 at h = 1 *)
P9 == \neg (dec[1][1] = 1 /\ dec[2][1] = 2)
=============================================================================

    