------------------------------ MODULE simple_2phase_sym_byz ------------------------------
EXTENDS Integers, FiniteSets, TLC
CONSTANT Proposal, N 
ASSUME N \in Nat

Node == 0..N-1

Message ==      [type : {"prepropose"}, sender : Node, p : Proposal]
           \cup [type : {"recv_prepropose"}, receiver : Node, p : Proposal]
           \cup [type : {"propose"}, sender : Node, p : Proposal]
           \cup [type : {"recv_propose"}, receiver : Node, p : Proposal]
           \cup [type : {"finished"}]
                       
VARIABLE p, dec, msgs

vars == <<p, dec, msgs>>

Max(s) == CHOOSE x \in s : \A y \in s : x >= y

Send(m) == msgs' = msgs \cup {m}

TypeOK == msgs \subseteq Message

Init == /\ p = [n \in Node |-> 0]
        /\ dec = [n \in Node |-> 0]
        /\ msgs = {}
 
SendPrePropose(i, pv) ==  LET preproposes_msgs == {m \in msgs : m.type = "prepropose"} IN
                          /\ Cardinality(preproposes_msgs) = 0    
                          /\ Send([type |-> "prepropose", sender |-> i, p |-> pv])
                          /\ p' = [ p EXCEPT ![i] = pv ]
                          /\ UNCHANGED <<dec>>
                          
RecvPrePropose(i) == \E m \in msgs :
                     /\ m.type = "prepropose"
                     /\ p' = [ p EXCEPT ![i] = m.p ]
                     /\ Send([type |-> "recv_prepropose", receiver |-> i, p |-> m.p])
                     /\ UNCHANGED <<dec>>
                     
SendPropose(i) == \E m \in msgs : 
                  /\ m.type = "recv_prepropose" /\ m.receiver = i /\ m.p = p[i]
                  /\ Send([type |-> "propose", sender |-> i, p |-> p[i]])
                  /\ UNCHANGED <<p, dec>>                   

RecvPropose(i) == LET proposes_msgs == {m \in msgs : m.type = "propose"} IN
                  LET propose_vals == {m.p : m \in proposes_msgs} IN  
                  /\ Cardinality(proposes_msgs) = N 
                  /\ dec' = [dec EXCEPT ![i] = Max(propose_vals)] 
                  /\ Send([type |-> "recv_propose", receiver |-> i, p |-> propose_vals])
                  /\ UNCHANGED <<p>>

Finished == LET recv_proposes_msgs == {m \in msgs : m.type = "recv_propose"} IN
            /\ Cardinality(recv_proposes_msgs) = N
            /\ Send([type |-> "finished"])
            /\ UNCHANGED <<p, dec>>

Finished2 == \E m \in msgs : m.type = "finished"
            
Next == Finished  
        \/ \E pv \in Proposal : 
        \/ \E i \in Node :  \/ SendPrePropose(i,pv) 
                            \/ RecvPrePropose(i)
                            \/ SendPropose(i) 
                            \/ RecvPropose(i)

WFRecvPropose == \A i \in Node : WF_<<>>(RecvPropose(i))
 
WFPropose == \A i \in Node : WF_<<>>(SendPropose(i))

Spec == Init /\ [][Next]_vars /\ WF_<<>>(Finished)

Agreed == \A i \in Node : \A j \in Node : dec[i] > 0 /\ dec[j] > 0 => dec[i] = dec[j]

P0 == \A i \in Node : <> (dec[i] > 0)
(* P0 == \A i \in Node : [] (<> <<RecvVote(i)>>_vars => <> (dec[i] > 0)) *)

(* P1 is false: there is a state where all dec[i] > 0 *)
P1 == \neg (\A i \in Node : \A j \in Node : dec[i] = dec[j] /\ dec[i] > 0) 

NotWFFinished == \neg (WF_<<>>(Finished))

P2 == \neg Finished2

P3 == [] <> Finished2 => ( \A i \in Node : <> (dec[i] > 0))
=============================================================================

