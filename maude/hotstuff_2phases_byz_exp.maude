load hotstuff_2phases_v0

mod BYZ-SCENARIO-EQUIVOCATION is
	protecting HOTSTUFF2-SEMANTICS .

	vars N S : Sender .
	eq byz(N) = if N <= nbyz then true else false fi .

	vars Curr_view : HView .
	ops byzMsgs : Sender HView -> Messages .
	ceq byzMsgs(S, Curr_view) =
 	  msg(S, "propose", Curr_view, -1, 10, emptyM) #
	  msg(S, "prepared", Curr_view, -1, 10, emptyM) #
	  msg(S, "committed", Curr_view, -1, 10, emptyM) #
	  msg(S, "propose", Curr_view, -1, 11, emptyM) #
	  msg(S, "prepared", Curr_view, -1, 11, emptyM) #
	  msg(S, "committed", Curr_view, -1, 11, emptyM)
	if byz(S) .

    eq byzMsgs(S, Curr_view) = emptyM [owise] .

    vars SS : HStates .
	var V : HView .
	var Msgs : Messages .

	*** (byz 1 and 2 send any possible msg for values 10 and 11 at view 1)
	rl [on_byz] : < SS, V, Msgs >  => < addMsgs(SS, byzMsgs(1, 1) # byzMsgs(2, 1)), V, Msgs > .
endm

smod BYZ-STRAT-EQUIVOCATION is
	 protecting BYZ-SCENARIO-EQUIVOCATION .
 	 protecting HOTSTUFF2-PREDS .
	 including NORMAL-EXEC .

	 strat equivocation : @ GlobalState .

	 ***(correct 3 and 4 commit diff vals)
	 sd equivocation :=
 	   newview+(1 # 2 # 3 # 4) ;
	   on_byz ;
	   on_propose ; broadcast ;
	   on_propose ; broadcast ;
	   on_prepared ; broadcast ; on_prepared ; broadcast ;
	   on_committed ; on_committed ;
	   idle .


endsm

*** srew gS using equivocation(10, 11) . *** ; print ! .

*** cex found
*** red modelCheck(gS, [] ~ (diffDecs) , 'equivocation) .


mod BYZ-SCENARIO-AMNESIA is
	protecting HOTSTUFF2-SEMANTICS .

	vars N S : Sender .
	ceq byz(N) = true if N == 3 or N == 4 .
	eq byz(N) = false [owise] .

	vars V Curr_view Prepared_view, Locked_view : HView .
	vars MVal Prepared_val Curr_val Dec_val : Val .
	vars Voted : Bool .
	vars Msgs Cert : Messages .
	vars MType : MsgType .
	ops byzMsg : Sender MsgType HView Val -> Message .
	ceq byzMsg(S, MType, V, MVal) = msg(S, MType, V, -1, MVal, emptyM) if byz(S) .

	ops byzMsgs1 byzMsgs2 : -> Messages .
	eq byzMsgs1 = byzMsg(3, "prepared", 1, 1) # byzMsg(3, "committed", 1, 1) # byzMsg(4, "committed", 1, 1) .
	eq byzMsgs2 = byzMsg(3, "prepared", 2, 2) # byzMsg(4, "prepared", 2, 2) # byzMsg(3, "committed", 2, 2) # byzMsg(4, "committed", 2, 2) .

	rl [on_byz1] :
	   << S, Curr_view, Prepared_view, Locked_view, Prepared_val, Curr_val, Dec_val, Cert, Voted, Msgs >> =>
	   << S, Curr_view, Prepared_view, Locked_view, Prepared_val, Curr_val, Dec_val, Cert, Voted, Msgs # byzMsgs1 >> .

	rl [on_byz2] :
	   << S, Curr_view, Prepared_view, Locked_view, Prepared_val, Curr_val, Dec_val, Cert, Voted, Msgs >> =>
	   << S, Curr_view, Prepared_view, Locked_view, Prepared_val, Curr_val, Dec_val, Cert, Voted, Msgs # byzMsgs2 >> .

endm

smod BYZ-STRAT-AMNESIA is
	 protecting BYZ-SCENARIO-AMNESIA .
 	 protecting HOTSTUFF2-PREDS .
	 including NORMAL-EXEC .
	 including STRATEGY-MODEL-CHECKER .
	 including LTL-SIMPLIFIER .

	 strat amnesia : @ GlobalState .

     vars S S1 S2 : Sender .

	 sd amnesia := newview+(1 # 2 # 3 # 4) ; on_byz1[S <- 1] ;
	 *** ( correct 1 and correct 2 are partitioned:
	    - 1 proposes 1 and byz 3 and 4 vote for 1 so 1 has QC for 1 but because 2 doesn't see this QC, it proposes its own value 2 )
	 	 newleaderB ;
		 propose+(1 # 2) ;
		 prepared+(1) ;
		 committed+(1) ;
		 *** 1 has a certificate for prepared_value = 1 so the Byz delay his message s.t. 2 cannot be aware of it and consequently proposes its own val
		 incr_gv ;
		 newview+(2 # 3 # 4) ; on_byz2[S <- 2] ;
		 newleaderB ;
		 propose+(2) ;
		 prepared+(2) ;
		 committed+(2) .

endsm

*** srew gS using amnesia . *** ; (matchrew < SS, V, Msgs > by Msgs using top(filter_prepare) ) . *** ; print .

*** cex found
*** red modelCheck(gS, [] ~ (diffDecs) , 'amnesia) .

quit .